// console.log(basicPrice.textContent);

document.addEventListener('DOMContentLoaded', function () {
    const toggle = document.getElementById("t1");
    const basicPrice = document.getElementById("p1");
    const professionalPrice = document.getElementById("p2");
    const masterPrice = document.getElementById("p3");
    const annuallyChange = document.getElementById('h1');
    const monthlyChange = document.getElementById('h2');
    toggle.addEventListener('input', function () {

        if (toggle.checked) {
            basicPrice.textContent = '$19.99';
            professionalPrice.textContent = '$24.99';
            masterPrice.textContent = '$39.99';
        }
        else {
            basicPrice.textContent = '$199.99';
            professionalPrice.textContent = '$249.99';
            masterPrice.textContent = '$399.99';


        }
    })
})
